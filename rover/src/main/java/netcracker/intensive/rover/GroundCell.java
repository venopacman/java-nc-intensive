package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

public class GroundCell {
    private CellState cell;

    public GroundCell(CellState cellState) {
        cell = cellState;
    }

    public CellState getState() {
        return cell;
    }

    public void setState(CellState cellState) {
        cell = cellState;
    }

}
