package netcracker.intensive.rover.stats;

import netcracker.intensive.rover.Point;

import java.util.HashSet;
import java.util.Set;

public class SimpleRoverStatsModule implements RoverStatsModule {
    private HashSet<Point> set;

    public SimpleRoverStatsModule() {
        set = new HashSet<>();
    }

    @Override
    public void registerPosition(Point position) {
        set.add(position);
    }

    @Override
    public boolean isVisited(Point point) {
        if (set.contains(point))
            return true;
        return false;
    }

    @Override
    public HashSet<Point> getVisitedPoints() {
        return set;
    }
}
