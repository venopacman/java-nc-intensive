package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.command.RoverCommand;

import java.util.*;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";
    private Collection<RoverCommand> commands;
    private Map<String,Object> settings;

    public Collection<RoverCommand> getCommands() {
        return commands;
    }

    public Map<String,Object> getSettings() {
        return settings;
    }
}
