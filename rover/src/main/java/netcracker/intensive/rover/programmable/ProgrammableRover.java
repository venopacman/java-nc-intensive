package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.GroundVisor;
import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;

import javax.naming.directory.Attribute;
import java.util.Map;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover {

    private Point currentPosition;
    private Map<String,Object> settings;

    public ProgrammableRover(GroundVisor visor, SimpleRoverStatsModule statsModule) {

    }

    public void executeProgramFile(String file) {

    }

    public Point getCurrentPosition() {
        return currentPosition;
    }

    public Map<String,Object> getSettings() {
        return settings;
    }
}
