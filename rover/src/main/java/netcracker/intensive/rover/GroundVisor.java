package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

public class GroundVisor {
    private Ground ground;

    public GroundVisor(Ground ground) {
        this.ground = ground;
    }

    public boolean hasObstacles(Point point) {
        try {
            if (ground.getCell(point.getX(), point.getY()).getState() == CellState.FREE)
                return false;
            else
                return true;
        } catch (OutOfGroundException e) {
            return false;
        }
    }

    public boolean inBounds(Point point) {
        if (point.getX() >= 0 && point.getY() >= 0 && point.getX() < ground.getWidth() && point.getY() < ground.getLength())
            return true;
        else
            return false;
    }
}
