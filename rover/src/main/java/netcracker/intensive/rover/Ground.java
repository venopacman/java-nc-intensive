package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;


public class Ground {
    private ArrayList<ArrayList<GroundCell>> cells;
    private int width, length;

    public int getWidth(){
        return width;
    }
    public int getLength(){
        return length;
    }
    public Ground(int width, int length) {
        // Количество "столбцов" на нашей карте
        this.width = width;
        // Количество "строк"
        this.length = length;

        cells = new ArrayList<ArrayList<GroundCell>>(width);
        ArrayList<GroundCell> coll;
        for (int i = 0; i < width; i++) {
            coll = new ArrayList<GroundCell>(length);
            for (int j = 0; j < length; j++) {
                coll.add(new GroundCell(CellState.FREE));
            }
            cells.add(coll);
        }
    }


    public GroundCell getCell(int x, int y) throws OutOfGroundException {
        if (x >= 0 && y >= 0 && x < width && y < length) {

            return cells.get(x).get(y);
        } else
            throw new OutOfGroundException();

    }

    public void initialize(GroundCell... groundCells) {
        if (groundCells.length < width * length) {
            throw new IllegalArgumentException();
        }
        ArrayList<GroundCell> arr = new ArrayList<GroundCell>(Arrays.asList(groundCells));
        Iterator<GroundCell> it = arr.iterator();
        for (int i = 0; i < cells.size(); i++) {
            for (int j = 0; j < cells.get(0).size(); j++) {
                cells.get(j).get(i).setState(it.next().getState());
            }
        }
    }
}