package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.Direction;

public class Rover implements Landable, Moveable, Liftable, Turnable {
    private Point currentPosition;
    private boolean airborne;
    private Direction direction;
    private GroundVisor groundVisor;


    public Rover(GroundVisor groundVisor, Direction direction, Point currentPosition) {
        this.groundVisor = groundVisor;
        this.direction = direction;
        this.currentPosition = currentPosition;
        this.airborne = false;
    }

    public Rover(GroundVisor groundVisor) {
        this.groundVisor = groundVisor;
        direction = Direction.SOUTH;
        currentPosition = new Point(0,0);
        airborne = false;
    }


    public void turnTo(Direction direction) {
        this.direction = direction;
    }

    public void move()  {
        if(airborne)
            return;
        Point feasiblePosition;
        switch (direction) {
            case EAST:
                feasiblePosition = new Point(currentPosition.getX() + 1, currentPosition.getY());


                break;
            case WEST:
                feasiblePosition = new Point(currentPosition.getX() - 1, currentPosition.getY());


                break;
            case SOUTH:
                feasiblePosition = new Point(currentPosition.getX(), currentPosition.getY() + 1);


                break;
            case NORTH:
                feasiblePosition = new Point(currentPosition.getX(), currentPosition.getY() - 1);


                break;
            default:
                feasiblePosition = currentPosition;
        }
        if (!groundVisor.inBounds(feasiblePosition)){
           lift();
        }
        else
        if (!groundVisor.hasObstacles(feasiblePosition))
        {
            currentPosition= feasiblePosition;
        }


    }

    public Point getCurrentPosition() {
        return currentPosition;
    }

    public boolean isAirborne() {
        return airborne;
    }

    public void lift() {
        airborne = true;
        currentPosition =null;
        direction=null;
    }

    public Direction getDirection() {
        return direction;
    }

    public void land(Point expectedPosition, Direction direction) {
        if(!groundVisor.inBounds(expectedPosition))
        return;
        if (!groundVisor.hasObstacles(expectedPosition)) {
            this.airborne = false;
            this.currentPosition = expectedPosition;
            this.direction = direction;
        }
    }
}
