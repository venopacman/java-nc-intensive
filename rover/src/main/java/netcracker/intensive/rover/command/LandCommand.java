package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.constants.Direction;
import netcracker.intensive.rover.programmable.ProgrammableRover;

public class LandCommand implements RoverCommand {

    public LandCommand(ProgrammableRover rover, Point point, Direction direction) {

    }

    public LandCommand(Rover rover, Point position, Direction direction) {

    }

    @Override
    public void execute() {

    }
}
