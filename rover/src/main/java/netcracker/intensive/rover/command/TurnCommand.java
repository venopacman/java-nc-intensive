package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.constants.Direction;
import netcracker.intensive.rover.programmable.ProgrammableRover;

public class TurnCommand implements RoverCommand {

    public TurnCommand(ProgrammableRover rover, Direction direction) {

    }

    public TurnCommand(Rover rover, Direction direction) {

    }

    @Override
    public void execute() {

    }
}
